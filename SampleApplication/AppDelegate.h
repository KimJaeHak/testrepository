//
//  AppDelegate.h
//  SampleApplication
//
//  Created by KIH on 2016. 3. 2..
//  Copyright (c) 2016년 KIH. All rights reserved.
//  일반 소스파일 커밋 테스트

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

